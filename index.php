<?php include 'header.php'; ?>
<body>
	<div class="container">
		<div class="row center">
			<div class="span12">
				<h2 class="short">Tugas <strong class="inverted"> Pemrograman </strong> Integratif </h2>
					<p class="featured lead">
						Nama : Antragama Ewa Abbas /18211058 <br>
						Nama : Gilang Ramadhan / 18211045 <br> <br>
									Membuat web service untuk membaca file XML. Data diambil dari tiga file dalam format yang berbeda, yaitu .CSV, .SQL dan XML itu sendiri. Langkah yang harus dipersiapkan : membuat web service, membaca client request.
					</p>
			</div>  <!-- penutup div span12  -->
		</div> <!-- penutup div row center  -->

		<div class="row">
			<center>
				<a href="xml_view.php" class="btn btn-large btn-primary pull-top" >File Xml</a>
				<a href="mysql_xml_view.php" class="btn btn-large btn-primary pull-top">File SQL</a>
				<a href="csv_xml_view.php" class="btn btn-large btn-primary pull-top">File CSV</a>
		
				
				<div class="space">
					<hr>
					<a href="informasi.php" class="btn btn-large btn-primary">Informasi Tugas 2</a>
					<br>
					<p class="featured lead">Menampilkan informasi IPK mahasiswa STI 2013</p>	
				</div> <!-- penutup div space -->
				
				<div class="space">
					<form action="informasi_xml.php">
					NIM : <input type ="text" name="nim" value="all">
					<input type="submit" value="submit">
					</form>

					<p class="featured lead">Untuk mendapatkan URI dari file XML<br>masukkan parameter dengan NIM : 18211001-18211007 atau NIM ="all"</p>

				</div> <!-- penutup div space -->

				<div class="tugastiga">
					<hr>
					<h2 class="short">Tugas <strong class="inverted"> Tiga </strong> Integratif </h2>
					
					<div class="kumpulan button">
						<a href="ubah_ketabel/harya.php" class="btn btn-large btn-primary pull-top">Harya</a>
						<a href="ubah_ketabel/fatoni.php" class="btn btn-large btn-primary pull-top">Fatoni</a>
						<a href="ubah_ketabel/gunawan.php" class="btn btn-large btn-primary pull-top">Gunawan</a>
						<a href="ubah_ketabel/gumarus.php" class="btn btn-large btn-primary pull-top">Gumarus</a>
						<a href="ubah_ketabel/yogidanang.php" class="btn btn-large btn-primary pull-top">Yogi</a>
						<a href="ubah_ketabel/Fajrin.php" class="btn btn-large btn-primary pull-top">Fajrin</a>
					</div>

				</div>				
			</center>
		</div><!-- penutup div row-->	
	</div> <!-- penutup div container-->

	<footer id="footer">
		<div class="footer-copyright">
			<div class="container">
				<div class="row">
                	<div class="col-md-1">
						<a href="#" class="logo">
							<img alt="Porto – Responsive HTML5 Template" src="css/logo-footer.png">
						</a>
					</div>
                
	                <div class="col-md-5 col-md-offset-1">
	                    <p> Copyright 2013 by Spyropress. All Rights Reserved.</p>				
	                </div>
                
	                <div class="col-md-4">
						<nav id="sub-menu" class="menu-footer-container"><ul id="menu-footer" class="nav"><li class="menu-faqs"><a href="#">FAQs</a></li>
						<li class="menu-sitemap"><a href="#">Sitemap</a></li>
						<li class="menu-contact"><a href="#">Contact</a></li>
						</ul></nav>				
					</div>
                </div>
		</div>
	</div>

	</footer>
</body>
</html>