Nama : Antragama Ewa Abbas /18211058
Nama : Gilang Ramadhan / 18211045
----------------------------------------
Untuk tugas 2 :
Untuk mendapatkan URI dari file XML
masukkan parameter dengan NIM : 18211001-18211007 atau NIM ="all"


Cara menampilkan data dapat dilakukan dengan cara sebagai berikut :
> Menampilkan semua data. Pada project ini, terdapat 3 buah data sumber : xml, php, dan sql
    >> pemrograman_integratif/xml.php?nim=all
    >> pemrograman_integratif/csv.php?nim=all
    >> pemrograman_integratif/sql.php?nim=all

> Menampilkan data berdasarkan nim tertentu
    >> pemrograman_integratif/xml.php?nim=<masukkan nim yang dicari>
    >> pemrograman_integratif/csv.php?nim=<masukkan nim yang dicari>
    >> pemrograman_integratif/mysql.php?nim=<masukkan nim yang dicari>

    Contoh
    >> pemrograman_integratif/xml.php?nim=18211014
    >> pemrograman_integratif/csv.php?nim=18211002
    >> pemrograman_integratif/mysql.php?nim=18211060

-------------------------

Untuk tugas 3, yang menggunakan web service secara benar adalah tombol "gunawan"..